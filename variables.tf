variable "container_name" {
  description = "Value of the name for the Docker container"
  type        = string
  default     = "teia_docusaurus"
}

variable "localhost_volume_path" {
  description = "Path to your local folder containing Docusaurus folder"
  type        = string
  default     = "/srv/teia/teia-staging-docusaurus/website"
}
