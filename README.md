# Docusaurus Gitlab Pages

Forked from:

* https://gitlab.com/pages/docusaurus

# Teia Wiki Docs

Checked out as a submodule here:

```
[submodule "website/docs"]
	path = website/docs
	url = https://github.com/teia-community/teia-docs.wiki.git
```

When cloning the repo, add the --recurse-submodules option to check out the
teia-docs.wiki repo. Or use `git submodule init` if you cloned it already.

# Terraform

I prefer Terraform over Docker Compose or using DockerFiles.

Terraform files are just here to build a container locally for testing.
