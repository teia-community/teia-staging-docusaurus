terraform {
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "3.0.1"
    }
  }
}

provider "docker" {
}

# Pulls the image
resource "docker_image" "docusaurus_container" {
  name = "awesometic/docusaurus"
}

# Create a container
resource "docker_container" "docusaurus_container" {
  image = docker_image.docusaurus_container.image_id
  name  = var.container_name
  volumes {
    container_path = "/docusaurus"
    host_path      = var.localhost_volume_path
    read_only      = false
  }
  ports {
    internal = 3000
    external = 3000
  }
}

# Create a volume
resource "docker_volume" "docusaurus_volume" {
  name = "docusaurus_volume"
}
